FROM python:3.9

COPY requirements.txt /

COPY ./app/ /app
COPY gunicorn_start.sh /gunicorn_start.sh

RUN pip install -r requirements.txt
RUN ls /bin

WORKDIR /app
ENTRYPOINT ["/gunicorn_start.sh"]
