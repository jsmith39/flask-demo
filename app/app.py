import uuid
from beaker.middleware import SessionMiddleware
from flask.sessions import SessionInterface
from flask import Flask, render_template, request, session, redirect
from flask_cas_wvu import CAS, login_required
from config import Config

app = Flask(__name__, static_url_path="/static")

# CAS client
cas = CAS()
cas.init_app(app)
app.config.from_object(Config)

# set up server-side sessions via Beaker
session_opts = {
    "session.type": "ext:redis",
    "session.url": Config.SESSION_URL,
    "session.invalidate_corrupt": True,
    "session.auto": True,
    "session.cookie_expires": Config.SESSION_LIFETIME or 600,
    "session.key": "wvu_self_service",
    "session.data_serializer": "json",
    "session.secret": Config.SECRET_KEY,
    "session.secure": Config.SESSION_SECURE,
    "session.httponly": Config.SESSION_HTTPONLY,
}


class BeakerSessionInterface(SessionInterface):
    """
    replace client side flask sessions with an interface to the server side beaker sessions
    """

    def open_session(self, beaker_app, beaker_request):
        """
        starts the session
        :param beaker_app:
        :param beaker_request:
        :return:
        """
        beaker_session = beaker_request.environ["beaker.session"]
        return beaker_session

    def save_session(self, beaker_app, beaker_session, beaker_response):
        """
        saves the session
        :param beaker_app:
        :param beaker_session:
        :param beaker_response:
        """
        beaker_session.save()

@app.before_request
def before_request():
    """
    run before each request
    """
    # no need to worry about static file requests
    if not request.path.startswith("/static/"):
        if session.get("_id", None) is None:
            session["_id"] = uuid.uuid4().hex


@app.route(f"/clear", strict_slashes=False, methods=["GET"])
def clear_entity():
    """
    Clear session by url
    :return: html
    """
    session.clear()
    return redirect("/")


@app.route("/", methods=["GET"])
def index_get():
    attrs = dict(
        a_variable="WOOHOO",
    )
    return render_template(template_name_or_list="index.html", **attrs)


@app.route("/protected", methods=["GET"])
@login_required
def protected_get():
    attrs = dict(
        cas_attributes=session.get("CAS_ATTRIBUTES"),
    )
    return render_template(template_name_or_list="protected.html", **attrs)


