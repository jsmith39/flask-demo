# -*- coding: utf-8 -*-
"""
Local configuration storage
THIS FILE SHOULD NOT BE COMMITTED TO GIT. WILL BE FOR DEMO PURPOSES
"""
import uuid


class Config(object):
    """Configuration object accessible within flask app"""

    # Flask app config
    INSTANCE = "dev"  # local-dev, dev, final, prod
    DEBUG = True  # True or False

    # Build request
    PREFERRED_URL_SCHEME = "http" if INSTANCE != "local-dev" else "http"
    PORT = ":8003" if INSTANCE != "local-dev" else ":5000"
    BASE_URL = f"{PREFERRED_URL_SCHEME}://flask-demo-1231781872.us-east-2.elb.amazonaws.com{PORT}" if INSTANCE != "local-dev" else f"{PREFERRED_URL_SCHEME}://127.0.0.1{PORT}"
    BASE_PATH = ""

    # Choose uuid4 for new sessions each restart, or a random string for persistent
    SECRET_KEY = uuid.uuid4().hex  # uuid.uuid4().hex or "A_STUPIDLY_LONG_RANDOM_STRING"

    # CAS
    CAS_SERVER = "https://sso.wvu.edu" if INSTANCE != "local-dev" else "https://ssodev.wvu.edu"  # Required
    CAS_LOGIN_ROUTE = "/cas/login"  # Required
    CAS_LOGOUT_ROUTE = "/cas/logout"  # Required
    CAS_AFTER_LOGIN = 'index'  # Required
    CAS_AFTER_LOGOUT = f"{BASE_URL}/clear"  # Required
    # CAS_RENEW = "true"  # true or comment out

    # SESSION
    SESSION_LIFETIME = 60000
    SESSION_SECURE = False
    SESSION_HTTPONLY = True
    SESSION_COOKIE_NAME = "wvu_demo"
    SESSION_DOMAIN = 'amazonaws.com'
    SESSION_URL = 'redis://default@shibcache.f5ncwe.ng.0001.use1.cache.amazonaws.com:6379'  # redis://[:password]@host[:port][/db]
