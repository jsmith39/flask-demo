output "registry_url" {
  value = aws_ecr_repository.flask-demo.repository_url
}

output "front_end_dns_name" {
  value = aws_lb.flask-demo.dns_name
}

