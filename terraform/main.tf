terraform {
}


provider "aws" {
  region = "us-east-2"
  default_tags {
    tags = {
      "wvu:tagVersion" = "20210426"
      "wvu:appId"      = "flask-demo"
      "wvu:contact"    = "jsmith39@wvu.edu"
      "wvu:zone"       = "sandbox"
    }
  }
}


locals {
  vpc-id = "vpc-6cdbcc05"
}

data "aws_subnets" "flask-demo" {
  filter {
    name   = "vpc-id"
    values = [local.vpc-id]
  }
}

##################################################
# EFS Volume and Access Point
##################################################
resource "aws_efs_file_system" "flask-demo-efs" {
  creation_token = "flask-demo-efs"

  tags = {
    "wvu:tagVersion" = "20210426"
    "wvu:appId"      = "flask-demo"
    "wvu:contact"    = "jsmtih39@wvu.edu"
    "wvu:zone"       = "sandbox"
  }
}

resource "aws_efs_access_point" "flask-demo-efs-accesspoint" {
  file_system_id = aws_efs_file_system.flask-demo-efs.id
  posix_user {
    gid = 1000
    uid = 1000
  }
  root_directory {
    path = "/mnt/efs"
    creation_info {
      owner_gid   = 1000
      owner_uid   = 1000
      permissions = 755
    }
  }
  tags = {
    Name             = "flask-demo-efs-accesspoint"
    "wvu:tagVersion" = "20210426"
    "wvu:appId"      = "flask-demo"
    "wvu:contact"    = "jsmith39@wvu.edu"
    "wvu:zone"       = "sandbox"
  }
}

resource "aws_efs_mount_target" "flask-demo-efs-mount" {
  file_system_id  = aws_efs_file_system.flask-demo-efs.id
  subnet_id       = "subnet-bc96ccc7"
  security_groups = [aws_security_group.efs_nfs.id]
}

##################################################
# Container Registry
##################################################
resource "aws_ecr_repository" "flask-demo" {
  name = "flask-demo"
}

##################################################
# Fargate ECS cluster
##################################################
resource "aws_ecs_cluster" "flask-demo" {
  name = "flask-demo"
}


##################################################
# Fargate ECS task definition
##################################################
resource "aws_ecs_task_definition" "flask-demo" {
  family                   = "flask-demo"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  volume {
    name = aws_efs_file_system.flask-demo-efs.creation_token
    efs_volume_configuration {
      file_system_id = aws_efs_file_system.flask-demo-efs.id
      root_directory = "/mnt/efs"
    }
  }
  container_definitions = jsonencode(
    [
      {
        name  = "flask-demo"
        image = "663733581889.dkr.ecr.us-east-2.amazonaws.com/flask-demo:latest"
        mountPoints = [{
          containerPath = "/mnt/efs"
          sourceVolume  = "flask-demo-efs"
          readOnly      = false
        }]
        portMappings = [{
          protocol      = "tcp"
          containerPort = 8003
          hostPort      = 8003
        }]

      }
    ]

  )
}

##################################################
# Fargate ECS Service
##################################################

resource "aws_ecs_service" "main" {
  name                               = "flask-demo"
  platform_version                   = "1.4.0"
  cluster                            = aws_ecs_cluster.flask-demo.id
  task_definition                    = aws_ecs_task_definition.flask-demo.arn
  desired_count                      = 2
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent         = 200
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"
  network_configuration {
    security_groups  = [aws_security_group.allow_http.id]
    subnets          = data.aws_subnets.flask-demo.ids
    assign_public_ip = true
  }
  load_balancer {
    target_group_arn = aws_alb_target_group.flask-demo.id
    container_name   = "flask-demo"
    container_port   = 8003
  }

}



##################################################
# IAM 
##################################################

#Task Execution Role
resource "aws_iam_role" "ecs_task_execution_role" {
  name = "flask-demo-ecsTaskExecutionRole"

  assume_role_policy = <<-EOF
	{
		"Version": "2012-10-17",
		"Statement": [
			{
				"Action": "sts:AssumeRole",
				"Principal": {
					"Service": "ecs-tasks.amazonaws.com"
				},
				"Effect": "Allow",
				"Sid": ""
			}
		]
	}
	EOF
}

resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}


##################################################
# Security Groups
##################################################
resource "aws_security_group" "allow_http" {
  description = "Allow HTTP inbound traffic"
  ingress {
    description = "HTTP from anywhere"
    from_port   = 8003
    to_port     = 8003
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "alb_sg" {
  description = "application load balancer security group"
}

resource "aws_security_group_rule" "egress_all" {
  type              = "egress"
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 0
  security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group_rule" "ingress_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group" "efs_nfs" {
  description = "nfs permissions for efs volume"
}

resource "aws_security_group_rule" "nfs_in" {
  type              = "ingress"
  from_port         = 2049
  to_port           = 2049
  protocol          = "tcp"
  cidr_blocks       = ["172.31.16.0/20"]
  security_group_id = aws_security_group.efs_nfs.id
}


##################################################
# ALB - Application Load Balancer
##################################################
resource "aws_lb" "flask-demo" {
  name               = "flask-demo"
  internal           = false
  load_balancer_type = "application"
  subnets            = data.aws_subnets.flask-demo.ids
  security_groups    = [aws_security_group.alb_sg.id]
}

resource "aws_alb_target_group" "flask-demo" {
  name        = "flask-demo"
  port        = 8003
  protocol    = "HTTP"
  vpc_id      = local.vpc-id
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "5"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "2"
  }
}

resource "aws_alb_listener" "http" {
  load_balancer_arn = aws_lb.flask-demo.id
  port              = 80
  protocol          = "HTTP"


  default_action {
    target_group_arn = aws_alb_target_group.flask-demo.id
    type             = "forward"
  }
}
