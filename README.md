# Flask-demo

This project contains POC/Demo quality code to demonstrate:

1. A hello world flask application.
2. Creating a docker image that executes the flask application using the gunicorn WSGI server.
3. Using terraform to deploy the infrastructure necessary to run the container image using the AWS ECS Fargate service.
4. A basic gitlab CI/CD pipeline use to automate the building of the container image and the deployment to AWS ECS Fargate.

Of note this demo does not include a pipeline for deploying the underlying infrastructure.  It was decided 

## Application overview

This application is a quintessential 'hello world' application using [flask](https://flask.palletsprojects.com/).  It simply returns a basic HTML page with the contents ''Hello World' when executed.

### Executing locally.

This application can be executed locally for testing purposes.  It is recommended to use a python virtual environment when doing so.

#### Creating a python virtual environment and installing requirements within

A virtual environment can be created by executing the following command:
```
python3 -m venv venv
```

Once created the virtual environment can be activated by executing the following command:
```
. ./venv/bin/activate
```

Once the virtual environment has been activated the requirements for this application can be installed by executing the following command:
```
pip3 install -r requirements.txt
```

### Executing locally

To execute the application using the built in flask web server execute the following commands:
```
cd app
flask run
```

To deactivate the virtual environment execute the `deactivate` command.


## Container overview

The official python:3.9 image is being used as our base image.  Additional information regarding this image can be found on the [Python official image](https://hub.docker.com/_/python) section of [docker hub](https://hub.docker.com).

Since this is an simple example we are using a single Dockerfile to build the image.  This Dockerfile performs the following steps to build a container image containing our application:

1. Copy requirements.txt into the root of the container filesystem.
2. Copy the contents of app/ into the /app directory of the continer filesystem.
3. Copy gunicorn_start.sh into the root of the container filesystem.
4. Execute pip install -r requirements.txt inside of the container.
5. Configures the working directory for the container to be /app.
6. Configures the entrypoint for the container to be ./gunicorn_start.sh.

### Building the container image

The following command can be used to build the container image locally:
```
docker build . -t flask-test:latest
```

### Running the container locally

The following command can be used to run the run the container locally:
```
docker run -p 8003:8003 flask-test:latest
```

This will map port 8003 in the container to port 8003 on your localhost.  Port 8003 is the port on which we have configured gunicorn to listen within the container.

Stopping the running container is a two step process.

First the Container ID must be obtained by executing the `docker ps` command.

For example:
```
$ docker ps
CONTAINER ID   IMAGE               COMMAND                CREATED         STATUS         PORTS                                       NAMES
29a9a02e5436   flask-test:latest   "/gunicorn_start.sh"   3 seconds ago   Up 3 seconds   0.0.0.0:8003->8003/tcp, :::8003->8003/tcp   hopeful_matsumoto
```

Once you have identified the correct container ID, the container can be stopped by executing the `docker kill $CONTAINER_ID` command.

For example:
```
docker kill 29a9a02e5436
```

## AWS Infrastructure overview

In addition to the basic flask app contained in this project, we also are including POC quality terraform configuration to support running this application using [AWS ECS Fargate](https://aws.amazon.com/fargate/) and an [Amazon Elastic Container Registry](https://docs.aws.amazon.com/AmazonECR/latest/userguide/what-is-ecr.html) to store our container images.  Specific details regarding the infrastructure being deployed can be found in the README.md located inside of the terraform directory of this project.


# Pipeline overview

A rudimentary gitlab CI/CD pipeline has been created for this project to demonstrate the automation of building and deploying a flak application using Fargate.

The pipeline performs the following actions:

1. Builds the container image.
2. Pushes the container image to ECR.
3. Deploys the container to Fargate.
